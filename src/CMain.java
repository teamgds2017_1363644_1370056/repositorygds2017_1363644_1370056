import java.util.Scanner;
import java.io.*;

public class CMain {
	// Rellenar con los datos de los dos alumnos que presentan la pr�ctica
	static String NombreAlumno1="Ehtisham";
	static String ApellidosAlumno1="Muhammad Ashraf";
	static String NIAAlumno1="1363644"; // NIA alumno1
	static String NombreAlumno2="Bernat";
	static String ApellidosAlumno2="Galceran Visa";
	static String NIAAlumno2="1370056"; // NIA alumno2

	static String[] NIAS={
		"1363644","1370056"		
	};

	static boolean NIACorrecto(String nia) {
		for (int i=0;i<NIAS.length; ++i) if (nia.equals(NIAS[i])) return true;
		return false;
	}

	public static class CSyntaxError extends Exception {
		private static final long serialVersionUID=2001L;
	    public CSyntaxError(String msg) {
	        super(msg);
	    }
	}
	static CInvoicing m_Invoicing;
	static int nLine=0;
	static void ProcesarNuevo(Scanner sl) throws Exception {
		String elemento=sl.next();
		if (elemento.equalsIgnoreCase("Cliente")) {
			String nombre=sl.next();
			int numero= sl.nextInt();
			if (numero<1) throw new Exception("numero de cliente menor que 1 " + numero);
			m_Invoicing.NewClient(new CClient(nombre,numero));
		} 
		else if (elemento.equalsIgnoreCase("Producto")) {
			String nombre=sl.next();
			int codigo= sl.nextInt();
			float price = sl.nextFloat();
			if (codigo<1) throw new Exception("codigo de producto menor que 1 " + codigo);
			m_Invoicing.NewProduct(new CProduct(nombre,codigo,price));
		}
		else if (elemento.equalsIgnoreCase("Factura")) {
			int numero=sl.nextInt();
			String nombreCliente= sl.next();
			if (numero<1) throw new Exception("numero de factura menor que 1 " + numero);
			m_Invoicing.NewInvoice(new CInvoice(numero,m_Invoicing.FindClientByName(nombreCliente)));			
		}
		else if (elemento.equalsIgnoreCase("Linea")) {
			int numeroFactura= sl.nextInt();
			String nombreProducto= sl.next();
			int quantity = sl.nextInt();
			CInvoice factura=m_Invoicing.FindInvoiceByNumber(numeroFactura);
			CProduct producto=m_Invoicing.FindProductByName(nombreProducto);
			factura.AddInvoiceLine(producto, quantity);			
		}
		else throw new CSyntaxError("Nuevo ...");
	}
	static void ProcesarModificar(Scanner sl) throws Exception {
		String elemento=sl.next();
		if (elemento.equalsIgnoreCase("Cliente")) {
			String nombreCliente=sl.next();
			CClient cliente=m_Invoicing.FindClientByName(nombreCliente);
			String campo=sl.next();
			if (campo.equalsIgnoreCase("Nombre")) {
				String nuevoNombre=sl.next();
				m_Invoicing.UpdateClient(cliente, nuevoNombre, cliente.m_Number);
			}
			else if (campo.equalsIgnoreCase("Numero")) {
				int nuevoNumero=sl.nextInt();
				m_Invoicing.UpdateClient(cliente, cliente.m_Name,nuevoNumero);				
			}
			else throw new CSyntaxError("Modificar Cliente ...");
		} 
		else if (elemento.equalsIgnoreCase("Producto")) {
			String nombreProducto=sl.next();
			CProduct producto=m_Invoicing.FindProductByName(nombreProducto);
			String campo=sl.next();
			if (campo.equalsIgnoreCase("Nombre")) {
				String nuevoNombre=sl.next();
				m_Invoicing.UpdateProduct(producto, nuevoNombre, producto.m_Code);
			}
			else if (campo.equalsIgnoreCase("Codigo")) {
				int nuevoCodigo=sl.nextInt();
				m_Invoicing.UpdateProduct(producto, producto.m_Name, nuevoCodigo);
			}
			else if(campo.equalsIgnoreCase("Price")){
				float new_price = sl.nextFloat();
				m_Invoicing.UpdateProductPrice(producto,producto.m_Name,new_price);
				
			}
			else throw new CSyntaxError("Modificar Producto ...");
		}
		else if (elemento.equalsIgnoreCase("Factura")) {
			int numeroFactura=sl.nextInt();
			CInvoice factura=m_Invoicing.FindInvoiceByNumber(numeroFactura);
			String campo=sl.next();
			if (campo.equalsIgnoreCase("Cliente")) {
				String nuevoCliente=sl.next();
				CClient cliente=m_Invoicing.FindClientByName(nuevoCliente);
				m_Invoicing.UpdateInvoiceHeader(factura,factura.m_Number,cliente);
			}
			else if (campo.equalsIgnoreCase("Numero")) {
				int nuevoNumero=sl.nextInt();
				m_Invoicing.UpdateInvoiceHeader(factura,nuevoNumero,factura.m_Client);
			}
			else throw new CSyntaxError("Modificar Factura ...");
		}		
	}
	static void ProcesarEliminar(Scanner sl) throws Exception {
		String elemento=sl.next();
		if (elemento.equalsIgnoreCase("Cliente")) {
			String nombreCliente=sl.next();
			CClient cliente=m_Invoicing.FindClientByName(nombreCliente);
			m_Invoicing.DeleteClient(cliente);
		} 
		else if (elemento.equalsIgnoreCase("Producto")) {
			String nombreProducto=sl.next();
			CProduct producto=m_Invoicing.FindProductByName(nombreProducto);
			m_Invoicing.DeleteProduct(producto);
		}
		else if (elemento.equalsIgnoreCase("Factura")) {
			int numeroFactura=sl.nextInt();
			CInvoice factura=m_Invoicing.FindInvoiceByNumber(numeroFactura);
			m_Invoicing.DeleteInvoice(factura);
		}		
		else if (elemento.equalsIgnoreCase("Linea")) {
			int numeroFactura= sl.nextInt();
			String nombreProducto= sl.next();
			CInvoice factura=m_Invoicing.FindInvoiceByNumber(numeroFactura);
			CProduct producto=m_Invoicing.FindProductByName(nombreProducto);
			m_Invoicing.DeleteProductFromInvoice(factura, producto);			
		}
		else throw new CSyntaxError("Eliminar ...");
	}
	static void ProcesarVer(Scanner sl)  throws Exception {
		String elemento=sl.next();
		if (elemento.equalsIgnoreCase("Clientes")) {
			System.out.print(nLine + " : Salida : ");
			m_Invoicing.m_Clients.Print(System.out);
			System.out.println();
		} 
		else if (elemento.equalsIgnoreCase("Productos")) {
			System.out.print(nLine + " : Salida : ");
			m_Invoicing.m_Products.Print(System.out);
			System.out.println();
		}
		else if (elemento.equalsIgnoreCase("Facturas")) {
			System.out.print(nLine + " : Salida : ");
			m_Invoicing.m_Invoices.Print(System.out);			
			System.out.println();
		}
		else throw new CSyntaxError("Ver ...");		
	}
		
		static void ProcesarListar(Scanner sl) throws Exception{
			
			String element = sl.next();
			
			if(element.equalsIgnoreCase("Facturas")){
				System.out.println("LISTADO DE FACTURAS MUEBLES JOSE");
				System.out.println("NUMERO DE FACTURA   CLIENTE		IMPORTE");
				m_Invoicing.m_Invoices.printFacturas();
				System.out.println("TOTAL:" + m_Invoicing.m_Invoices.TotalFacturas());
			} else if (element.equalsIgnoreCase("Clientes")){
				System.out.println("LISTADO DE CLIENTES MUEBLES JOSE");
				System.out.println("NUMERO DE CLIENTE   NOMBRE");
				m_Invoicing.m_Clients.printClients();	
			}else if (element.equalsIgnoreCase("Productos")){
				System.out.println("LISTADO DE PRODUCTOS MUEBLES JOSE");
				System.out.println("CODIGO PRODUCTO   NOMBRE   PRECIO PRODUCTO");
				m_Invoicing.m_Products.printProducts();
			}
			else throw new CSyntaxError("Listado ...");
			
		}
	
	public static void main(String[] args) throws Exception {
		//System.out.println(NIAAlumno1);
		//System.out.println(NombreAlumno1);
		//System.out.println(ApellidosAlumno1);
		//System.out.println(NIAAlumno2);
		//System.out.println(NombreAlumno2);
		//System.out.println(ApellidosAlumno2);
		
		if (!NIACorrecto(NIAAlumno1)) throw new Exception("El NIA " + NIAAlumno1 + " no es de alumno matriculado");
		if (!NIACorrecto(NIAAlumno2)) throw new Exception("El NIA " + NIAAlumno2 + " no es de alumno matriculado");

		m_Invoicing=new CInvoicing();
		if (args.length!=1) {
			System.out.println("Falta el nombre del fichero de hjrdendes");
			return;
		}
		String filename=args[0];
		System.out.println("Fichero de ordenes: " + filename);
		try {
			File ordenes=new File(filename);
			Scanner s;
			s = new Scanner(ordenes);
			while (s.hasNextLine()) {
				try {
					++nLine;
					String linea = s.nextLine();
					System.out.println(nLine + " : Linea : " + linea);
					Scanner sl = new Scanner(linea);
					//sl.useDelimiter("\\s*");
					try {
						String orden=sl.next();
						if (orden.equalsIgnoreCase("Nuevo")) ProcesarNuevo(sl);
						else if (orden.equalsIgnoreCase("Nueva")) ProcesarNuevo(sl);
						else if (orden.equalsIgnoreCase("Modificar")) ProcesarModificar(sl);
						else if (orden.equalsIgnoreCase("Eliminar")) ProcesarEliminar(sl);
						else if (orden.equalsIgnoreCase("Ver")) ProcesarVer(sl);
						else if (orden.equalsIgnoreCase("Listado")) ProcesarListar(sl);
						else {
							sl.close();
							throw new CSyntaxError("Orden no reconocida " + linea);
						}
					}
					catch (java.util.NoSuchElementException e) {
						throw new CSyntaxError("Error de sintaxis: " + linea);
					}
				}
				catch (Exception e) {
					System.out.println(nLine + " : Excepcion : "+ e.toString());
					//e.printStackTrace();
				}
				
				catch (AssertionError e)  {
					System.out.println(nLine + " : Assert : "+ e.toString());
					//e.printStackTrace();		
				}
			}
			s.close();
		} 
		catch (Exception e) {
			System.out.println("Excepci�n no controlada");
			e.printStackTrace();
		}
	}
}
