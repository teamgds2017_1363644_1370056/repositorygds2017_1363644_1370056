
public class CInvoiceLineList extends CList {
	
	public void PushBack(CInvoiceLine e){
		super.PushBack(e);
	}
	
	public float TotalLines(){
		float total_Lines = 0;
		CNode node = m_Start;
		while(node != null){
			CInvoiceLine line = (CInvoiceLine) node.m_Element;
			total_Lines += line.product.m_Price * line.quantity;
			node = node.m_Next;
		}
		return total_Lines;
	}

}
