import java.io.PrintStream;

public class CInvoice extends CElement {
	public int m_Number;
	public CClient m_Client;
	public CInvoiceLineList m_InvoiceLines;
	
	public CInvoice(int number,CClient client) {
		m_Number=number;
		m_Client=client;
		m_InvoiceLines = new CInvoiceLineList();
	}
	public void AddInvoiceLine(CProduct product, int quantity) {
		CInvoiceLine line = new CInvoiceLine(product,quantity);
		m_InvoiceLines.PushBack(line);
	}
	public void DeleteProduct(CProduct product) {
		m_InvoiceLines.Delete(product);
	}
	public void Print(PrintStream out) {
		out.print("Invoice(");
		out.print(m_Number);
		out.print(",");
		out.print(m_Client.m_Name);
		out.print(",");
		m_InvoiceLines.Print(out);
		out.print(")");
	}
}
