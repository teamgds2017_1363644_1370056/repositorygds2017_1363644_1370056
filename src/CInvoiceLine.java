
import java.io.PrintStream;
public class CInvoiceLine extends CElement {
	public CProduct product;
	public int quantity;
	
	public CInvoiceLine(CProduct product, int quantity){
		this.product = product;
		this.quantity = quantity;
	}
	
	public void Print(PrintStream out){
		out.print("CInvoiceLine(");
		out.print(product.m_Name);
		out.print(",");
		out.print(quantity);
		out.print(")");
		
	}

}
