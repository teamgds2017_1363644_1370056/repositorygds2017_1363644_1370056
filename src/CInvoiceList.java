public class CInvoiceList extends CList {
	public CInvoice SearchByNumber(int number) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CInvoice) node.m_Element).m_Number==number) {
				return (CInvoice) node.m_Element;
			}
			node=node.m_Next;
		}
		return null;
	}
	public void PushBack(CInvoice e) {
		super.PushBack(e);
	}
	public boolean ClientHasInvoices(CClient client) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CInvoice) node.m_Element).m_Client==client) {
				return true;
			}
			node=node.m_Next;
		}
		return false;
	}	
	public boolean ProductInInvoices(CProduct product) {
		CNode node=m_Start;
		while (node!=null) {
			CInvoice invoice=(CInvoice) node.m_Element;
			if (invoice.m_InvoiceLines.MemberP(product)) {
				return true;
			}
			node=node.m_Next;
		}
		return false;
	}
	
	public float TotalFacturas(){
		
		float total_facturas = 0;
		CNode node = m_Start;
		while(node != null){
			CInvoice invoice = (CInvoice) node.m_Element;
			total_facturas += invoice.m_InvoiceLines.TotalLines();
			node = node.m_Next;
		}
		return total_facturas;
	}
	
	
	public void printFacturas(){
		CNode node = m_Start;
		while(node != null){
			CInvoice invoice = (CInvoice) node.m_Element;
			System.out.println(invoice.m_Number + "\t\t    " + invoice.m_Client.m_Name + "\t\t" + invoice.m_InvoiceLines.TotalLines());
			node = node.m_Next;
		}
	}
	
}
